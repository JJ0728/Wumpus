using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WumpusTest
{
    class GraphicsEngine {
        //Private variables
        private String hazards;
        private bool wumpus;
        private int score, room, tunnels;
        
        public GraphicsEngine(string hazards, bool wumpus, int score, int room, int tunnels){
            //Constructor
            this.hazards = hazards; this.wumpus = wumpus;
            this.score = score; this.room = room; this.tunnels = tunnels;
        }
        public String returnPlayerInfo() {
            //Returns information about the player
            return "Room: " + room +
                   "\nScore: " + score + 
                   "\nTunnels: " + tunnels;
        }
        public String returnInventory() {
            //Returns a list of the player's inventory (use player object)
            return "";
        }
        public String returnActions() {
            //Returns a list of actions a player could take
            if (wumpus) { return ""; }
            return "";
        }
    }
}