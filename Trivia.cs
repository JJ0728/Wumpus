using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WumpusTest
{
    public class Trivia
    {
        //Fields to keep track of trivia information such as the list of questions,
        //list of hints, counter, etc. 
        private Question[] list;
        private String[] hints;
        private int place;
        private int numHints;
        //Global variable for length of questions
        private int LISTLENGTH = 10;
        public Trivia()
        {
            this.place = 0;
            this.list = new Question[LISTLENGTH];
            try
            {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.
                using (StreamReader sr = new StreamReader("trivia.txt"))
                {
                    string line;
                    int count = 0;
                    int count2 = 0;
                    for (int a = 0; a < LISTLENGTH; a++)
                    {
                        list[a] = new Question();
                    }

                // Read and display lines from the file until the end of 
                // the file is reached.
                while ((line = sr.ReadLine()) != null)
                    {
                        if(count > 5)
                        {
                            count2++;
                            count -= 6;
                        }
                        if (count % 5 == 0 && count != 0)
                        {
                            list[count2].setAnswer(line);
                        }
                        else if (count % 4 == 0 && count != 0)
                        {
                            list[count2].setOptionD(line);
                        }
                        else if (count % 3 == 0 && count != 0)
                        {
                            list[count2].setOptionC(line);
                        }
                        else if (count % 2 == 0 && count != 0)
                        {
                            list[count2].setOptionB(line);
                        }
                        else if (count % 1 == 0 && count != 0)
                        {
                            list[count2].setOptionA(line);
                        }
                        else
                        {
                            list[count2].setQuestion(line);
                        }
                        count++;
                    }
                }
            }
            catch (Exception e)
            {
                // Let the user know what went wrong.

                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);

            }
            this.hints = new String[LISTLENGTH];
            using (StreamReader sr = new StreamReader("hints.txt"))
            {
                String line;
                int count = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    this.hints[count] = line;
                    count++;
                }
            }
        }
        //Method to provide a user with a trivia question, reads from a file. 
        public String askQuestion()
        {
            //Determines which question to ask the user from a list of questions (array).
            string result = list[place].getQuestion() + "\n" + list[place].getOptionA() + "\n" + 
                list[place].getOptionB() + "\n" +  list[place].getOptionC() + "\n" + 
                list[place].getOptionD() + "\n";
            place++;
            //Returns the previously determined question.
            return result;
        }
        //Method to provide the user with a hint towards the game.
        public String giveHint()
        {
            //Random number generator (RNG) to randomize the hints given. 
            Random rand = new Random();
            String hint;     
            //Conditional to evaluate whether the user has used up all the hints. 
            if (numHints <= this.hints.Length - 1)
            {
                int num = rand.Next(this.hints.Length - (1 + numHints));
                hint = this.hints[num];
                for (int a = num; a < this.hints.Length - (1 + numHints); a++)
                {
                    this.hints[a] = this.hints[a + 1];
                }
            }
            else
            {
                //Reaching this point in code means that no more hints are left to be given. 
                hint = "There are no more hints!";
            }
            //Counter to check how many hints have been used up by the player. 
            numHints++;
            return hint;
        }
        //Method to evaluate the user input to a trivia question. 
        public bool checkAnswer(String input)
        {
            input.ToLower();
            //Cross checking the user answer with the correct answer. 
            if (input.Equals(list[place-1].getAnswer()))
            {
                //Reaching this point in the method means the user has provided a correct answer.
                return true;
            }
            else
            {
                //Reaching this point in the method means the user has provided an incorrect answer.
                return false;
            }
        }
    }
}
