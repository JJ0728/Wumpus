﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Rishi Kavikondala
// Team: 
// Hunt the Wumpus
// Game Control Class

namespace WumpusTest
{
    public class GameControl
    {
        private Boolean startGame = false;
        // Game Control constructor
        public GameControl()
        {
            this.startGame = true;
            if(startGame)
            {
                control();
            }
        }
        // The overarching game control method 
        public void control() { }
        // Takes in any user input, processes it, and redistributes it to other methods 
        public void takeInput() { }
        public void accessCave() { }
        public void accessMap() { }
        public void accessSound() { }
        public void accessHScore() { }
        public void displayScore() { }
        public void mainMenu() { }
    }
}